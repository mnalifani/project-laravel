@extends('layout.master')

@section('judul')
    Detail Kategori
@endsection


@section('content')
    <h1>{{$kategori->nama}}</h1>
    <p>{{$kategori->descripsi}}</p>
    <a href="/kategori" class="btn btn-info btn-sm">kembali</a>
@endsection
