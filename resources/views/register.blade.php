@extends('layout.master')


@section('judul')
Buat Acount Baru!
@endsection

@section('content')
<form action="/kirim" method="post">
    @csrf
    <label >First Name</label><br>
    <input type="text" name="first"><br>
    <label >Last Name</label><br>
    <input type="text" name="last"><br><br>
    <label >Jenis Kelamin</label><br>
    <input type="radio" name="jk" value="1">Laki-laki
    <input type="radio" name="jk" value="2">Perempuan<br><br>
    <label >Nationality</label><br>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="singapura">Singapura</option>
        <option value="malaysia">Malaysia</option>
        <option value="australia">Australia</option></select
    ><br /><br />
    <label>Language Spoken:</label><br />
    <input type="checkbox" name="language" value="1"/>Bahasa Indonesia <br />
    <input type="checkbox" name="language" value="2"/>English <br />
    <input type="checkbox" name="language" value="3"/>Other <br /><br />

    <label>Bio:</label><br /><br />
    <textarea name="bio" cols="30" rows="10"></textarea><br />

    <input type="submit" value="kirim">

</form> 
@endsection

    
