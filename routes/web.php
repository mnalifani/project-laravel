<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\KategoriController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'regis']);
Route::get('/welcome', [AuthController::class, 'welcome']);
Route::post('/kirim', [AuthController::class, 'kirim']);
Route::get('/data-table', [AuthController::class, 'dataTable']);
Route::get('/table', [AuthController::class, 'table']);


//CRUD Kategori

//FormCreate
Route::get('/kategori/create', [KategoriController::class, 'create']);
//tambah data ke database
Route::post('/kategori', [KategoriController::class, 'store']);

//Read
//tampil semua data
Route::get('kategori', [KategoriController::class, 'index']);
//detail kategori berdasar ID
Route::get('kategori/{kategori_id}', [KategoriController::class, 'show']);

//update
//form updat
Route::get('/kategori/{kategori_id}/edit', [KategoriController::class, 'edit']);
//update data ke database berdasrkan id
Route::put('/kategori/{kategori_id}', [KategoriController::class, 'update']);

//Delete
//delete berdasar id
Route::delete('/kategori/{kategori_id}', [KategoriController::class, 'destroy']);
