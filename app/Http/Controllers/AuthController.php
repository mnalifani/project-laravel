<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('register');
    }

    public function welcome(){
        return view('welcome');
    }

    public function dataTable(){
        return view('datatable');
    }

    public function table(){
        return view('table');
    }

    public function kirim(Request $request){
        $first = $request['first'];
        $last = $request['last'];
        $jenisKelamin = $request['jk'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];     

        return view('welcome', ['first'=>$first,'last'=>$last,'jenisKelamin'=>$jenisKelamin,
        'nationality'=>$nationality, 'language'=>$language, 'bio'=>$bio] );
    }
}
